<?php

/**
 * @file
 * Contains \Drupal\circles\CircleInterface.
 */

namespace Drupal\circles;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Circle entities.
 *
 * @ingroup circles
 */
interface CircleInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

}
