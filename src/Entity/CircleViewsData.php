<?php

/**
 * @file
 * Contains \Drupal\circles\Entity\Circle.
 */

namespace Drupal\circles\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Circle entities.
 */
class CircleViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['circle']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Circle'),
      'help' => $this->t('The Circle ID.'),
    );

    return $data;
  }

}
