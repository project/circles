<?php

/**
 * @file
 * Contains \Drupal\circles\CircleAccessControlHandler.
 */

namespace Drupal\circles;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Circle entity.
 *
 * @see \Drupal\circles\Entity\Circle.
 */
class CircleAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view circle entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit circle entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete circle entities');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add circle entities');
  }

}
