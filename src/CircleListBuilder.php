<?php

/**
 * @file
 * Contains \Drupal\circles\CircleListBuilder.
 */

namespace Drupal\circles;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Circle entities.
 *
 * @ingroup circles
 */
class CircleListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Circle ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\circles\Entity\Circle */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $this->getLabel($entity),
      new Url(
        'entity.circle.edit_form', array(
          'circle' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
