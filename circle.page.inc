<?php

/**
 * @file
 * Contains circle.page.inc..
 *
 * Page callback for Circle entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Circle templates.
 *
 * Default template: circle.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_circle(array &$variables) {
  // Fetch Circle Entity Object.
  $circle = $variables['elements']['#circle'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
